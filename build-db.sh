#! /usr/bin/bash


echo "###########################"
echo "Building the repo database."
echo "###########################"

cd x86_64
rm -f joca-arch-repo*
repo-add -n -R joca-arch-repo.db.tar.gz *.pkg.tar.zst
rm joca-arch-repo.db
rm joca-arch-repo.db.sig
rm joca-arch-repo.files
rm joca-arch-repo.files.sig

mv joca-arch-repo.db.tar.gz joca-arch-repo.db
mv joca-arch-repo.db.tar.gz.sig joca-arch-repo-db.sig
mv joca-arch-repo.files.tar.gz joca-arch-repo.files
mv joca-arch-repo.files.tar.gz.sig joca-arch-repo.files.sig

echo "#######################################"
echo "Packages in the repo have been updated!"
echo "#######################################"
